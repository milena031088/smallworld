using SmallWorld.src.controllers;
using SmallWorld.src.interfaces;
using SmallWorld.src.views;
using SmallWorld.src.model.animalsType;//Dependencia estructural!
using SmallWorld.src.model.dietType;//Dependencia estructural!
using SmallWorld.src.model.foodType;//Dependencia estructural!
using System.Reflection.Metadata;

namespace SmallWorld
{
    public partial class Form1 : Form
    {
        private IController animalController = AnimalController.GetController();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void BtnAnimals_Click(object sender, EventArgs e)
        {
            CRUDView cRUDView = new();
            cRUDView.ShowDialog();
        }
    }
}