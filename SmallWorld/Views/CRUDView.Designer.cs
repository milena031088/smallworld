﻿namespace SmallWorld.src.views
{
    partial class CRUDView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            TxtSpecie = new TextBox();
            label1 = new Label();
            label2 = new Label();
            TxtWeight = new TextBox();
            label3 = new Label();
            TxtAge = new TextBox();
            label4 = new Label();
            CbbDiet = new ComboBox();
            BtnCreateAnimal = new Button();
            GbAnimalCreation = new GroupBox();
            DgvAnimals = new DataGridView();
            GbAnimalCreation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)DgvAnimals).BeginInit();
            SuspendLayout();
            // 
            // TxtSpecie
            // 
            TxtSpecie.BackColor = Color.FromArgb(64, 64, 64);
            TxtSpecie.ForeColor = Color.FromArgb(224, 224, 224);
            TxtSpecie.Location = new Point(60, 27);
            TxtSpecie.Name = "TxtSpecie";
            TxtSpecie.Size = new Size(164, 23);
            TxtSpecie.TabIndex = 0;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(7, 30);
            label1.Name = "label1";
            label1.Size = new Size(47, 15);
            label1.TabIndex = 1;
            label1.Text = "Specie :";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(7, 59);
            label2.Name = "label2";
            label2.Size = new Size(51, 15);
            label2.TabIndex = 3;
            label2.Text = "Weight :";
            // 
            // TxtWeight
            // 
            TxtWeight.BackColor = Color.FromArgb(64, 64, 64);
            TxtWeight.ForeColor = Color.FromArgb(224, 224, 224);
            TxtWeight.Location = new Point(60, 56);
            TxtWeight.Name = "TxtWeight";
            TxtWeight.Size = new Size(55, 23);
            TxtWeight.TabIndex = 2;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(121, 59);
            label3.Name = "label3";
            label3.Size = new Size(34, 15);
            label3.TabIndex = 5;
            label3.Text = "Age :";
            // 
            // TxtAge
            // 
            TxtAge.BackColor = Color.FromArgb(64, 64, 64);
            TxtAge.ForeColor = Color.FromArgb(224, 224, 224);
            TxtAge.Location = new Point(161, 56);
            TxtAge.Name = "TxtAge";
            TxtAge.Size = new Size(63, 23);
            TxtAge.TabIndex = 4;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new Point(20, 88);
            label4.Name = "label4";
            label4.Size = new Size(34, 15);
            label4.TabIndex = 7;
            label4.Text = "Diet :";
            // 
            // CbbDiet
            // 
            CbbDiet.BackColor = Color.FromArgb(64, 64, 64);
            CbbDiet.FlatStyle = FlatStyle.Flat;
            CbbDiet.ForeColor = Color.FromArgb(224, 224, 224);
            CbbDiet.FormattingEnabled = true;
            CbbDiet.Location = new Point(60, 85);
            CbbDiet.Name = "CbbDiet";
            CbbDiet.Size = new Size(164, 23);
            CbbDiet.TabIndex = 8;
            // 
            // BtnCreateAnimal
            // 
            BtnCreateAnimal.FlatStyle = FlatStyle.Flat;
            BtnCreateAnimal.ForeColor = Color.FromArgb(224, 224, 224);
            BtnCreateAnimal.Location = new Point(120, 128);
            BtnCreateAnimal.Name = "BtnCreateAnimal";
            BtnCreateAnimal.Size = new Size(104, 26);
            BtnCreateAnimal.TabIndex = 9;
            BtnCreateAnimal.Text = "Create";
            BtnCreateAnimal.UseVisualStyleBackColor = true;
            BtnCreateAnimal.Click += BtnCreateAnimal_Click;
            // 
            // GbAnimalCreation
            // 
            GbAnimalCreation.Controls.Add(CbbDiet);
            GbAnimalCreation.Controls.Add(BtnCreateAnimal);
            GbAnimalCreation.Controls.Add(TxtSpecie);
            GbAnimalCreation.Controls.Add(label1);
            GbAnimalCreation.Controls.Add(label4);
            GbAnimalCreation.Controls.Add(TxtWeight);
            GbAnimalCreation.Controls.Add(label3);
            GbAnimalCreation.Controls.Add(label2);
            GbAnimalCreation.Controls.Add(TxtAge);
            GbAnimalCreation.ForeColor = Color.FromArgb(224, 224, 224);
            GbAnimalCreation.Location = new Point(12, 12);
            GbAnimalCreation.Name = "GbAnimalCreation";
            GbAnimalCreation.Size = new Size(235, 166);
            GbAnimalCreation.TabIndex = 10;
            GbAnimalCreation.TabStop = false;
            GbAnimalCreation.Text = "Animal";
            // 
            // DgvAnimals
            // 
            DgvAnimals.BorderStyle = BorderStyle.None;
            DgvAnimals.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            DgvAnimals.Location = new Point(253, 20);
            DgvAnimals.Name = "DgvAnimals";
            DgvAnimals.RowTemplate.Height = 25;
            DgvAnimals.Size = new Size(494, 461);
            DgvAnimals.TabIndex = 11;
            // 
            // CRUDView
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.FromArgb(64, 64, 64);
            ClientSize = new Size(759, 493);
            Controls.Add(DgvAnimals);
            Controls.Add(GbAnimalCreation);
            ForeColor = Color.FromArgb(224, 224, 224);
            FormBorderStyle = FormBorderStyle.None;
            Name = "CRUDView";
            Text = "CRUDView";
            GbAnimalCreation.ResumeLayout(false);
            GbAnimalCreation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)DgvAnimals).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private TextBox TxtSpecie;
        private Label label1;
        private Label label2;
        private TextBox TxtWeight;
        private Label label3;
        private TextBox TxtAge;
        private Label label4;
        private ComboBox CbbDiet;
        private Button BtnCreateAnimal;
        private GroupBox GbAnimalCreation;
        private DataGridView DgvAnimals;
    }
}