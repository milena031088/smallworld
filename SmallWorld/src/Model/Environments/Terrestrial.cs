﻿using SmallWorld.src.interfaces;
using SmallWorld.src.Interfaces;
using SmallWorld.src.Model.Terrains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallWorld.src.model.animalsType
{
    internal class Terrestial : IEnvironment
    {
        public bool CanMoveThrough(ITerrain terrain)
        {
            return terrain is Land;
        }
    }
}
