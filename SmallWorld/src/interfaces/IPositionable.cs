﻿using SmallWorld.src.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Intrinsics;
using System.Text;
using System.Threading.Tasks;

namespace SmallWorld.src.interfaces
{
    internal interface IPositionable
    {
        public int GetPosX();
        public int GetPosY();
        public bool Move(ITerrain terrain);
    }
}
