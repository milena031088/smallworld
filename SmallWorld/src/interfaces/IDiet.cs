﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallWorld.src.interfaces
{
    internal interface IDiet
    {
        bool CanEat(IFood food);
    }
}
